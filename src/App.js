import { Route, Routes } from "react-router-dom";

import AllMeetupsPage from "./pages/AllMeetups";
import FavoritesPage from "./pages/Favorites";
import NewMeetupsPage from "./pages/NewMeetups";
import Layout from "./components/layouts/Layout";

function App() {
	return (
		<Layout>
			<Routes>
				<Route exact path="/" element={<AllMeetupsPage />} />
				<Route exact path="/new-meetup" element={<NewMeetupsPage />} />
				<Route exact path="/favorites" element={<FavoritesPage />} />
			</Routes>
		</Layout>
	);
}

export default App;
